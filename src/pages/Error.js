import React from 'react';
import { Link } from 'react-router-dom';

export default function Error() {
	return(
		<>
			<h1>404 Page Not Found</h1>
			<p>Sorry, this page doesn't exist.</p>
			<h5>Go back to the <Link to="/">homepage</Link></h5>
		</>
		)
}