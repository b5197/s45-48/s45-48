import { useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const navigate = useNavigate()

	const { user, setUser } = useContext(UserContext);

	//state hooks to store the values of the input fields
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobileNumber, setMobileNumber ] = useState('');

	//state for the enable/disable button
	const [ isActive, setIsActive ] = useState(true);
	useEffect(() => {
			if((email !== '' && password !== '' && verifyPassword !=='') && (password === verifyPassword)){
				setIsActive(true)
			} else {
				setIsActive(false);
			}
		}, [email, password, verifyPassword])

	function registerUser(e) {
		e.preventDefault();
		fetch('http://localhost:4000/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNumber: mobileNumber,
				password: password,
				verifyPassword: verifyPassword
			})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined) {
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				})
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'You successfully registered!'
				})
				//get user's details from our token
				fetch('http://localhost:4000/users/register', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data.isAdmin === true) {
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							isAdmin: data.isAdmin
						})
						//push to the /courses
						navigate('/courses')
					} else {
						//if not an admin, push to '/' (homepage)
						navigate('/')
					}
				})
			} else {
				Swal.fire({
					title: 'Ooopsss!',
					icon: 'error',
					text: 'Something went wrong. Check your credentials.'
				})
			}
			setEmail('')
			setPassword('')
			setVerifyPassword('');
			setLastName('');
			setFirstName('');
			setMobileNumber('');
		})

		// //clear input fields
		// setEmail('');
		// setPassword('');
		// setVerifyPassword('');
		// setLastName('');
		// setFirstName('');
		// setMobileNumber('');

		// //
		// Swal.fire({
		//   title: 'Success!',
		//   text: 'Your registration is successful!',
		//   icon: 'success'
		// })
		})
	}

	return(
		(user.accessToken !== null) ?
		<Navigate to="/courses" />
		:
		<Form onSubmit={e => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group className='mb-4'>
				<Form.Label>First Name</Form.Label>
				<Form.Control
                    type="text"
                    placeholder="Enter first name"
                    required
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className='mb-4'>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
                    type="text"
                    placeholder="Enter last name"
                    required
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className='mb-4'>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
                    type="email"
                    placeholder="Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group className='mb-4'>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
                    type="number"
                    placeholder="Enter mobile number"
                    required
                    value={mobileNumber}
                    onChange={e => setMobileNumber(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className='mb-4'>
				<Form.Label>Password</Form.Label>
				<Form.Control
                    type="password"
                    placeholder="Enter password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className='mb-4'>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
                    type="password"
                    placeholder="Verify password"
                    required
                    value={verifyPassword}
                    onChange={e => setVerifyPassword(e.target.value)}
				/>
			</Form.Group>
			{ isActive ? 
			<Button variant="primary" type="submit" className="mt-3">Submit</Button>
			:
			<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
		</Form>
		)
}