import React from 'react';

//it creates a context object
//a context object, as the name states is a data type of an object than can be used to store information that can be shared to other components within the app
//we used this to avoid the use of prop-drilling
const UserContext = React.createContext();

//provider component -> allows the other components to consume/use the context object or supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;